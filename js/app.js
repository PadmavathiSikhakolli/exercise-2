(function () {
  var taskInput = document.getElementById("new-task");
  var addButton = document.getElementsByTagName("button")[0];
  var incompleteTasksHolder = document.getElementById("incomplete-tasks");
  var completedTasksHolder = document.getElementById("completed-tasks");

  var SET_ITEM = "set";
  var SET_DELETE = "delete";
  var SET_COMPLETE = "complete";
  var SET_INCOMPLETE = "incomplete";
  var SET_UPDATE = "update";
  var SET_STORAGE_TODO = "todo";
  var SET_STORAGE_COMPLETED = "completed";
  var createNewTaskElement = function (taskString, arr) {
    var listItem = document.createElement("li");
    var checkBox = document.createElement("input");
    var label = document.createElement("label");
    var editInput = document.createElement("input");
    var editButton = document.createElement("button");
    var deleteButton = document.createElement("button");

    checkBox.type = "checkbox";
    editInput.type = "text";
    editButton.innerText = "Edit";
    editButton.className = "edit";
    deleteButton.innerText = "Delete";
    deleteButton.className = "delete";
    label.innerText = taskString;

    listItem.appendChild(checkBox);
    listItem.appendChild(label);
    listItem.appendChild(editInput);
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton);
    return listItem;
  };

  var addTask = function (storageValue) {
    var listItemName = taskInput.value;
    if (listItemName.trim() == "") {
      taskInput.classList.add("error");
      document.getElementById("new-task").focus();
      return;
    } else {
      taskInput.classList.remove("error");
    }
    var listItem = createNewTaskElement(listItemName);
    setLocalStorage(SET_ITEM, SET_STORAGE_TODO, listItemName);
    incompleteTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
    taskInput.value = "";
  };

  var editTask = function () {
    var listItem = this.parentNode;
    var editInput = listItem.querySelectorAll("input[type=text")[0];
    var containsClass = listItem.classList.contains("editMode");
    var label = listItem.querySelector("label");
    if (containsClass) {
      var listItemName = editInput.value;
      if (listItemName.trim() == "") {
        alert("Todo-field is empty");
        editInput.classList.add("error");
        editInput.focus();
        return;
      } else {
        editInput.classList.remove("error");
        setLocalStorage(
          SET_UPDATE,
          SET_STORAGE_TODO,
          editInput.value,
          label.innerText
        );
      }
    }
    var button = listItem.getElementsByTagName("button")[0];
    if (containsClass) {
      label.innerText = editInput.value;
      button.innerText = "Edit";
    } else {
      editInput.value = label.innerText;
      button.innerText = "Save";
    }
    listItem.classList.toggle("editMode");
  };
  var deleteTask = function (el) {
    var listItem = this.parentNode;
    var ul = listItem.parentNode;
    ul.removeChild(listItem);
    setLocalStorage(
      SET_DELETE,
      SET_STORAGE_TODO,
      listItem.querySelector("label").innerText
    );
  };
  var taskCompleted = function (el) {
    var listItem = this.parentNode;
    completedTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskIncomplete);
    setLocalStorage(
      SET_COMPLETE,
      SET_STORAGE_TODO,
      listItem.querySelector("label").innerText
    );
  };
  var taskIncomplete = function () {
    var listItem = this.parentNode;
    incompleteTasksHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
    setLocalStorage(
      SET_INCOMPLETE,
      SET_STORAGE_TODO,
      listItem.querySelector("label").innerText
    );
  };
  var bindTaskEvents = function (taskListItem, checkBoxEventHandler, cb) {
    var checkBox = taskListItem.querySelectorAll("input[type=checkbox]")[0];
    var editButton = taskListItem.querySelectorAll("button.edit")[0];
    var deleteButton = taskListItem.querySelectorAll("button.delete")[0];
    editButton.onclick = editTask;
    deleteButton.onclick = deleteTask;
    checkBox.onchange = checkBoxEventHandler;
    checkBox.onkeypress = function (e) {
      if ((e.keyCode ? e.keyCode : e.which) == 13) {
        checkBox.click();
      }
    };
  };
  addButton.addEventListener("click", addTask);

  for (var i = 0; i < incompleteTasksHolder.children.length; i++) {
    bindTaskEvents(incompleteTasksHolder.children[i], taskCompleted);
  }

  for (var i = 0; i < completedTasksHolder.children.length; i++) {
    bindTaskEvents(completedTasksHolder.children[i], taskIncomplete);
  }
  function setLocalStorage(functionType, name, value, oldvalue = "") {
    if (typeof Storage !== "undefined") {
      if (functionType == SET_ITEM) {
        var tasks = getLocalstorage();
        var todoupdate = [
          ...tasks,
          {
            name: value,
            completed: false,
            editmode: false,
            deleted: false,
          },
        ];
        //localStorage.setItem("tasks", JSON.stringify(updatedtasks));
        setLocalstorage(todoupdate);
      } else if (functionType == SET_DELETE) {
        getTodos()(value, "deleted", true);
      } else if (functionType == SET_COMPLETE) {
        getTodos()(value, "completed", true);
      } else if (functionType == SET_INCOMPLETE) {
        getTodos()(value, "completed", false);
      } else if (functionType == SET_UPDATE) {
        var tasks = getLocalstorage();
        tasks.find((val) => val.name == oldvalue).deleted = true;
        var todoupdate = [
          ...tasks,
          {
            name: value,
            completed: false,
            editmode: false,
            deleted: false,
          },
        ];
        setLocalstorage(todoupdate);
      }
    }
  }
  //event-listeners
  document.addEventListener("DOMContentLoaded", function (event) {
    var incompleteTasksHolder = document.getElementById("incomplete-tasks");
    var completedTasksHolder = document.getElementById("completed-tasks");
    if (
      typeof Storage !== "undefined" &&
      localStorage.getItem("tasks") != null
    ) {
      incompleteTasksHolder.innerHTML = "";
      completedTasksHolder.innerHTML = "";

      var todo = getLocalstorage();
      if (todo) {
        var todoFiltered = todo.filter(
          (val) => val.completed == false && val.deleted == false
        );
        todoFiltered.map((val) => {
          listItem = createNewTaskElement(val.name);
          incompleteTasksHolder.appendChild(listItem);
          bindTaskEvents(listItem, taskCompleted);
        });
        var completed = todo.filter(
          (val) => val.completed == true && val.deleted == false
        );
        completed.map((val) => {
          var completedCont = `<li><input type="checkbox" tabindex="0" checked><label>${val.name}</label><input type="text"><button class="edit">Edit</button><button class="delete">Delete</button></li>`;
          var HTMLStr = stringToHTML(completedCont);
          completedTasksHolder.appendChild(HTMLStr);
          bindTaskEvents(HTMLStr, taskIncomplete);
        });
      }
    }
    if (getLocalstorage() == null) {
      var tasks = [];
      //incomplete task holder
      for (var i = 0; i < incompleteTasksHolder.children.length; i++) {
        if (incompleteTasksHolder.children[i].classList.contains("editMode")) {
          tasks = [
            ...tasks,
            {
              name: document
                .getElementById("incomplete-tasks")
                .children[i].querySelector("label").innerText,
              completed: false,
              editmode: true,
              deleted: false,
            },
          ];
          setLocalstorage(tasks);
        } else {
          tasks = [
            ...tasks,
            {
              name: document
                .getElementById("incomplete-tasks")
                .children[i].querySelector("label").innerText,
              completed: false,
              editmode: false,
              deleted: false,
            },
          ];
          setLocalstorage(tasks);
        }
      }
      //complete task holder
      for (var i = 0; i < completedTasksHolder.children.length; i++) {
        if (completedTasksHolder.children[i].classList.contains("editMode")) {
          tasks = [
            ...tasks,
            {
              name: completedTasksHolder.children[i].querySelector("label")
                .innerText,
              completed: true,
              editmode: true,
              deleted: false,
            },
          ];
          setLocalstorage(tasks);
        } else {
          tasks = [
            ...tasks,
            {
              name: completedTasksHolder.children[i].querySelector("label")
                .innerText,
              completed: true,
              editmode: false,
              deleted: false,
            },
          ];
          setLocalstorage(tasks);
        }
      }
    } else {
      var tasks = getLocalstorage();
      //remove-incompleteTask
      var removeItem = [];
      for (var i = 0; i < incompleteTasksHolder.children.length; i++) {
        var listItem = incompleteTasksHolder.children[i];
        var taskText = listItem.querySelector("label").innerText;
        if (
          tasks.findIndex((a) => a.name == taskText && a.completed == true) !=
          -1
        ) {
          removeItem.push(listItem);
        }
      }
      for (var j = 0; j < removeItem.length; j++) {
        removeItem[j].remove();
      }
      //remove-completeTask
      var removeItemCompleted = [];
      for (var i = 0; i < completedTasksHolder.children.length; i++) {
        var listItem = completedTasksHolder.children[i];
        var taskText = listItem.querySelector("label").innerText;
        if (
          tasks.findIndex((a) => a.name == taskText && a.completed == false) !=
          -1
        ) {
          removeItemCompleted.push(listItem);
        }
      }
      for (var j = 0; j < removeItemCompleted.length; j++) {
        removeItemCompleted[j].remove();
      }
    }
  });
  var stringToHTML = function (str) {
    var parser = new DOMParser();
    var documentParser = parser.parseFromString(str, "text/html");
    return documentParser.body.getElementsByTagName("li")[0];
  };
  function getTodos() {
    var tasks = getLocalstorage();
    return (value, node, valuetobe) => {
      tasks.find((val) => val.name == value)[node] = valuetobe;
      setLocalstorage(tasks);
    };
  }
  function setLocalstorage(tasks) {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }
  function getLocalstorage() {
    return JSON.parse(localStorage.getItem("tasks"));
  }
})();
